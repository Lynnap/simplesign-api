// import cors from 'cors';
import { handleError } from '../helpers/error'

class Server {
    constructor({ express, routes, cookieParser, csrfMiddleware, handleError }) {
        this.app = express();
        this.initializeBodyParsing(express);
        this.initializeMiddlewares({ cookieParser, csrfMiddleware });
        this.initializeApplicationRouter(routes);

        this.app.use((err, req, res, next) => {
            handleError(err, res);
        });
    }

    initializeBodyParsing(express) {
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(express.json());
        // this.app.use(
        //   cors({
        //     origin: 'http://localhost:1234',
        //     credentials: true,
        //   })
        // );
    }

    initializeMiddlewares({ cookieParser, csrfMiddleware, morgan }, logger) {
        this.app.use(cookieParser());
        this.app.get('/csrf', csrfMiddleware, (req, res) => {
            res.status(200).json(req.csrfToken());
        });
        // this.app.get(morgan('combined', { stream: logger.stream }))
    }

    initializeApplicationRouter(routes) {
        this.app.use(routes);
    }

    listen(port) {
        this.app.listen(port, async () => console.log(`application started on port : ${port}`));
    }
}

export default Server;
