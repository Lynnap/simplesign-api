import { ApiError } from '../../helpers/error';
import UserEntity from './userEntity';

class UserService {
    constructor({ userRepository }) {
        this.userRepo = userRepository;
        this.apiError = ApiError
    }

    async getAll() {
        const users = await this.userRepo.findAll();
        return users.map((user) => new UserEntity(user));
    }
    async registerLearner(userData) {
        const userEntity = new UserEntity(userData);
        if (!userEntity.validateLearner())
            throw new this.apiError(400, 'User entity validation error: Missing parameters');

        const newUser = await this.userRepo.createLearner(userEntity);
        return new UserEntity(newUser);
    }
    async registerTrainer(userData) {
        const userEntity = new UserEntity(userData);
        if (!userEntity.validateTrainer())
            throw new this.apiError(400, 'User entity validation error: Missing parameters');

        const newUser = await this.userRepo.createTrainer(userEntity);
        return new UserEntity(newUser);
    }
    async registerAdmin(userData) {
        const userEntity = new UserEntity(userData);
        if (!userEntity.validateAdmin())
            throw new this.apiError(400, 'User entity validation error: Missing parameters');

        const newUser = await this.userRepo.createAdmin(userEntity);
        return new UserEntity(newUser);
    }

    async login(userData) {
        const userEntity = new UserEntity(userData);
        if (!userEntity.validateLogin())
            throw new this.apiError(403, 'User entity validation error: Not authorized');
        const user = await this.userRepo.findByEmail(userEntity);
        if (!user) throw new this.apiError(400, 'Account do not exist');

        const passwordMatch = await this.userRepo.compareHash(
            userEntity.password,
            user.password
        );
        if (!passwordMatch) throw new this.apiError(400, 'Password do not match');

        return new UserEntity(user);
    }

    async me(userId) {
        const user = await this.userRepo.findById(userId);
        if (!user) throw new this.apiError(400, 'Account do not exist');
        return new UserEntity(user);
    }
}

export default UserService;