class UserController {
    constructor({ userService, jwtService }) {
        this.userService = userService;
        this.jwt = jwtService;
    }

    getAll = async ({ res }) => {
        try {
            let users = await this.userService.getAll();
            res.send('hello moto');
            // res.status(200).json(users);
        } catch (err) {
            next(err);
        }
    }

    registerLearner = async (req, res, next) => {
        try {
            const user = await this.userService.registerLearner({ ...req.body });
            res.status(201).json(user);
        } catch (err) {
            next(err);
        }
    }
    registerTrainer = async (req, res, next) => {
        try {
            const user = await this.userService.registerTrainer({ ...req.body });
            res.status(201).json(user);
        } catch (err) {
            next(err);
        }
    }
    registerAdmin = async (req, res, next) => {
        try {
            const user = await this.userService.registerAdmin({ ...req.body });
            res.status(201).json(user);
        } catch (err) {
            next(err);
        }
    }

    login = async (req, res, next) => {
        try {
            const user = await this.userService.login({ ...req.body });
            const token = await this.jwt.generateToken({ id: user.id });
            res.cookie('auth-cookie', token, { expires: false, httpOnly: true });
            res.status(200).json(user);
        } catch (err) {
            next(err);
        }
    };


}

export default UserController;