class UserEntity {
    constructor({ id, lastname, firstname, email, password, role }) {
        this.id = id;
        this.lastname = lastname;
        this.firstname = firstname;
        this.email = email;
        this.password = password;
        this.role = role;
    }

    validateLearner() {
        if (!this.email) return false;
        else return true;
    }
    validateTrainer() {
        if (!this.email) return false;
        if (!this.role || this.role !== 'trainer') return false;
        else return true;
    }
    validateAdmin() {
        if (!this.email) return false;
        if (!this.role || this.role !== 'admin') return false;
        else return true;
    }

    validateLogin() {
        if (!this.email || !this.password) return false;
        else return true;
    }
}

export default UserEntity;
