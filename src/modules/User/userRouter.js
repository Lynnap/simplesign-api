class UserRouter {
    constructor({ router, auth, csrfMiddleware, userController, }) {
        this.router = router;
        this.initializeRoutes({ auth, csrfMiddleware, userController });
        return this.router;
    }

    initializeRoutes({ auth, csrfMiddleware, userController }) {
        this.router
            .route('/user')
            .get(auth.authenticate, userController.getAll)
        this.router
            .route('/register/learner')
            .post(userController.registerLearner)
        this.router
            .route('/register/trainer')
            .post(userController.registerTrainer)
        this.router
            .route('/register/admin')
            .post(userController.registerAdmin)
        this.router
            .route('/login')
            .post(userController.login)
        this.router.post('/test', auth.authenticate, csrfMiddleware, (req, res) => {
            res.json('ça marche !')
        })

    }
}

export default UserRouter;
