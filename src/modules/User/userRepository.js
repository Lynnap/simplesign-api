import bcrypt from 'bcrypt';
class UserRepository {
    constructor({ userDao, bcrypt }) {
        this.userDao = userDao;
        this.bcrypt = bcrypt;
    }

    async findAll() {
        return await this.userDao.findAll();
    }
    async createLearner(userEntity) {
        const salt = bcrypt.genSaltSync(10);
        userEntity.password = bcrypt.hashSync(userEntity.password, salt);
        userEntity.role = 'learner';
        return await this.userDao.create(userEntity);
    }
    async createTrainer(userEntity) {
        const salt = bcrypt.genSaltSync(10);
        userEntity.password = bcrypt.hashSync(userEntity.password, salt);
        userEntity.role = 'trainer';
        return await this.userDao.create(userEntity);
    }
    async createAdmin(userEntity) {
        const salt = bcrypt.genSaltSync(10);
        userEntity.password = bcrypt.hashSync(userEntity.password, salt);
        userEntity.role = 'admin';
        return await this.userDao.create(userEntity);
    }

    async findByEmail(userEntity) {
        return await this.userDao.findOne({ where: { email: userEntity.email } });
    }
    async findById(userId) {
        // return await this.userDao.findByPk(userId);
        return await this.userDao.findOne({ where: { id: userId } });
    }
    compareHash = async (password, hash) =>
        await this.bcrypt.compareSync(password, hash);
}

export default UserRepository;