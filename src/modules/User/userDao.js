import { Model, DataTypes, Sequelize } from 'sequelize';
import db from '../../config/database';

class UserDao extends Model {
    static init(sequelize) {
        return super.init(
            {
                id: { autoIncrement: true, primaryKey: true, type: DataTypes.INTEGER },
                lastname: { type: DataTypes.STRING, allowNull: false },
                firstname: { type: DataTypes.STRING, allowNull: false },
                email: { type: DataTypes.STRING, allowNull: false },
                password: { type: DataTypes.STRING, allowNull: false },
                role: {
                    type: DataTypes.ENUM('admin', 'trainer', 'learner'),
                    defaultValue: 'learner',
                    allowNull: false,
                },
            },

            { sequelize, modelName: "User" }
        );
    }

    static associate(models) {
        // define association here
    }
};

UserDao.init(db.sequelize);
export default UserDao;